# polybar-theme

polybar themes for i3-wm 

inspired by Pro themes for Awesome WM 

## Video
https://youtu.be/qejfoAS12Ow <br />
[![Archlabs ](https://img.youtube.com/vi/qejfoAS12Ow/0.jpg)](https://www.youtube.com/watch?v=qejfoAS12Ow)



## Preview

## dark
![dark](/preview/pro-dark.png)
<br />
## light
![light](/preview/pro-light.png)
<br />
## gotham
![gotham](/preview/pro-gotham.png)
<br />
## medium-light
![medium-light](/preview/pro-medium-light.png)
<br />
## medium-dark
![medium-dark](/preview/pro-medium-dark.png)
<br />
## extra
![extra](/preview/extra.png)


## Fonts
- **terra**
- **waffle**
- **Material**
- **feather**
- **Iosevka Nerd Font**

### Other prerequisites for polybar module
* `io.elementary.music, vlc, spotify`
* `cava`
* `i3lock, betterlockscreen`

## Details
- **Distro** archlabs ;)
- **WM** i3-gaps-rounded & qtile
- **Panel** Polybar
- **Terminal** Alacritty
- **Music Player** io.elementary.music & vlc,
- **Text Editor** vim
- **File Manager** Ranger, with w3m image previewer
- **Alternative File Manager** Thunar
- **Screen Recorder** simplescreenrecorder
- **Program Launcher** rofi
- **Info Fetcher** Neofetch
- **GTK Theme** arc-theme-gotham
- **Icons** la-capitaine
- **CLI Shell Intepreter** fish
- **Compositor** picom
- **Notification Daemon** dunst

## add this line to your ~/.config/i3/config

## exec --no-startup-id ~/.config/polybar/panels/polybar-wrapper.sh &

## color-palette: gotham



- ** Special Thanks
- ** stobenski: [awesome pro themes](https://github.com/stobenski/pro)
- ** adi1090x: [polybar themes](https://github.com/adi1090x/polybar-themes)
